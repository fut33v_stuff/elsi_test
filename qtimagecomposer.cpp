#include "qtimagecomposer.h"

QtImageComposer::QtImageComposer() {

}

QImage QtImageComposer::composeTwoImages(QImage firstImage, QImage secondImage) {
    QImage result = firstImage;
    QPainter p(&result);

    p.setCompositionMode(QPainter::CompositionMode_SourceOver);
    p.setOpacity(OPACITY);
    p.drawImage(0, 0, secondImage);

    return result;
}
