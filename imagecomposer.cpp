#include "imagecomposer.h"

ImageComposer::ImageComposer() {

}

QImage ImageComposer::compose(std::vector<QImage> images) {
    QImage result = images[0];

    if (images.size() == 2) {
        result = composeTwoImages(result, images[1]);
    } else {
        for (size_t i = 0; i < images.size() - 1; i++) {
            result = composeTwoImages(result, images[i+1]);
        }
    }
    return result;
}

QImage ImageComposer::composeThreaded(std::vector<QImage> images) {
    std::vector<ImageComposerThread*> threads;
    size_t x;

    if (images.size() % 2 == 0) {
        x = images.size();
    } else {
        x = images.size() - 1;
        size_t lastElement = images.size() - 1;
        ImageComposerThread::appendImage(images[lastElement], this);
    }

    for (size_t i = 0; i < x; i+=2){
        ImageComposerThread* ct = new ImageComposerThread(this, images[i], images[i+1]);
        QObject::connect(ct, SIGNAL(imageReady(QImage)),
             this, SLOT(onImageReady(QImage)),
             Qt::DirectConnection);
        threads.push_back(ct);
    }
    for (auto i = threads.begin(); i < threads.end(); i++) {
        (*i)->start();
    }
    for (auto i = threads.begin(); i < threads.end(); i++) {
        (*i)->wait();
    }

    return ImageComposerThread::resultImage;
}

void ImageComposer::onImageReady(QImage image) {
    ImageComposerThread::appendImage(image, this);
}

/* ------------------------------ */
bool ImageComposerThread::isFirst = true;
QMutex ImageComposerThread::mutex;
QImage ImageComposerThread::resultImage;

ImageComposerThread::ImageComposerThread(ImageComposer *c, QImage i1, QImage i2) :
       composer(c), firstImage(i1), secondImage(i2) {

}

void ImageComposerThread::run(){
    QImage result;
    result = composer->composeTwoImages(firstImage, secondImage);
    imageReady(result);
}

void ImageComposerThread::appendImage(QImage image, ImageComposer* composer) {
    mutex.lock();
    if (isFirst) {
        resultImage = image;
        isFirst = false;
    } else {
        resultImage = composer->composeTwoImages(resultImage, image);
    }
    mutex.unlock();
}

std::vector<QImage> convertImagesTo(std::vector<QImage> images, QImage::Format format) {
    for (auto i = images.begin(); i < images.end(); i++) {
        *i = i->convertToFormat(format);
    }
    return images;
}
