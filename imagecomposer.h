#ifndef IMAGECOMPOSER_H
#define IMAGECOMPOSER_H

#include <vector>

#include <QImage>
#include <QDebug>
#include <QThread>
#include <QMutex>

#define OPACITY .5


class ImageComposer : public QObject {
    Q_OBJECT
public:
    ImageComposer();

    QImage compose(std::vector<QImage>);
    QImage composeThreaded(std::vector<QImage>);

    virtual QImage composeTwoImages(QImage, QImage) = 0;
public slots:
    void onImageReady(QImage);
};


class ImageComposerThread : public QThread {
    Q_OBJECT
public:
    ImageComposerThread(ImageComposer* c, QImage i1, QImage i2);

    void run();

    static void appendImage(QImage, ImageComposer *composer);
private:
    ImageComposer* composer;
    QImage firstImage;
    QImage secondImage;
public:
    static bool isFirst;
    static QImage resultImage;
    static QMutex mutex;
signals:
    void imageReady(QImage);
};

std::vector<QImage> convertImagesTo(std::vector<QImage> images, QImage::Format format);

#endif // IMAGECOMPOSER_H
