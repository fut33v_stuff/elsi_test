#include "wimagesgrid.h"

wImagesGrid::wImagesGrid(QWidget *parent) : QWidget(parent) {
    this->setLayout(&layout);
}

void wImagesGrid::setImages(std::vector<QImage> images) {
    // clean layout
    QLayoutItem* item;
    while ((item = layout.takeAt(0)) != NULL) {
        delete item->widget();
        delete item;
    }

    std::vector<QImage> imagesThumbs = images;

    int row = 0, column = 0;
    for (auto i = imagesThumbs.begin(); i < imagesThumbs.end(); i++) {
        float c = SCALE_COEFF;
        if (i->height() > 2000 || i->width() > 2000) {
            c = SCALE_COEFF * .5;
        }
        *i = i->scaled(i->width()*c, i->height()*c);


        QLabel* l = new QLabel(this);
        l->setPixmap(QPixmap::fromImage(*i));
        layout.addWidget(l, row, column);

        if (column == PICTURES_GRID_COLUMNS) {
            column = 0;
            row++;
        } else {
            column++;
        }
    }

    for (auto i = images.begin(); i < images.end(); i++) {
        *i = i->scaled(CONTENT_WIDTH, CONTENT_HEIGHT);
    }
}

void wImagesGrid::onImagesReady(std::vector<QImage> images) {
    setImages(images);
}
