#include "wcomposemanager.h"

wComposeManager::wComposeManager(QWidget *parent) :
        QWidget(parent) {

    buttonChooseFolder = new QPushButton(&buttons);
    buttonCompose = new QPushButton(&buttons);
    buttonSaveFile = new QPushButton(&buttons);

    buttonsLayout = new QBoxLayout(QBoxLayout::TopToBottom);
    buttonsLayout->addWidget(buttonChooseFolder);
    buttonsLayout->addWidget(buttonCompose);
    buttonsLayout->addWidget(buttonSaveFile);
    buttonsLayout->setGeometry(QRect(0, 0, 100, 70));
    buttons.setLayout(buttonsLayout);
    buttons.setMaximumHeight(100);
    buttons.setMaximumWidth(200);

    buttonChooseFolder->setText("Choose Folder");

    buttonCompose->setText("Compose");
    buttonCompose->setEnabled(false);

    buttonSaveFile->setText("Save file");
    buttonSaveFile->setEnabled(false);

    layout.addWidget(&buttons, 0, 0);
    layout.addWidget(&resultLabel, 0, 1);
    layout.addWidget(&imagesGrid, 2, 0, 1, 2);

    this->setLayout(&layout);
    QObject::connect(
        buttonChooseFolder, SIGNAL(clicked()),
        &imagesFolder, SLOT(chooseDirectory()));

    QObject::connect(
        &imagesFolder, SIGNAL(directoryChosen()),
        this, SLOT(onDirectoryChosen()));

    QObject::connect(
        this, SIGNAL(imagesReady(std::vector<QImage>)),
        &imagesGrid, SLOT(onImagesReady(std::vector<QImage>)));

    QObject::connect(
        buttonCompose, SIGNAL(clicked()),
        this, SLOT(onMergeButtonClick()));

    QObject::connect(
        buttonSaveFile, SIGNAL(clicked()),
        this, SLOT(onSaveFileButton()));

    QObject::connect(
        this, SIGNAL(imagesReady(std::vector<QImage>)),
        this, SLOT(onImagesReady()));

    QObject::connect(
        this, SIGNAL(imagesComposed(QImage)),
        this, SLOT(onImagesComposed(QImage)));

}

void wComposeManager::onDirectoryChosen() {
    images = imagesFolder.getGrayscaleImages();
    if (images.size()) {
        emit imagesReady(images);
    }
}

void wComposeManager::onMergeButtonClick() {
    images = imagesFolder.getGrayscaleImages();

    for (auto i = images.begin(); i < images.end(); i++) {
        *i = i->scaled(SCALE_TO, SCALE_TO);
    }

    QImage composedImage;
    MyImageComposer c;
    composedImage = c.composeThreaded(images);

    emit imagesComposed(composedImage);

    this->composedImage = composedImage;

}

void wComposeManager::onSaveFileButton() {
    QString fileName = QFileDialog::getSaveFileName(
        this, "Save file", ".", "Images (*.png *.jpg)"
    );

    QFileInfo fi(fileName);
    const char* format;
    if (fi.suffix() == "") {
        fileName += ".png";
        format = "PNG";
    } else {
        if (fi.suffix() == "jpg") {
            format = "JPG";
        }
        if (fi.suffix() == "png") {
            format = "PNG";
        }
    }

    composedImage.save(fileName, format);
}

void wComposeManager::onImagesComposed(QImage image){
    resultLabel.setPixmap(QPixmap::fromImage(image));
    buttonSaveFile->setEnabled(true);
}

void wComposeManager::onImagesReady() {
    buttonCompose->setEnabled(true);
}
