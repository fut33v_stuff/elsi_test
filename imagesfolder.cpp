#include "imagesfolder.h"

ImagesFolder::ImagesFolder() {

}

ImagesFolder::ImagesFolder(QString folderPath) :
    folderPath(folderPath) {

}

void ImagesFolder::setPath(QString folderPath) {
    folderImages.clear();
    this->folderPath = folderPath;
}

std::vector<QImage> ImagesFolder::getImages() {
    QDir dir;
    QStringList entryList;

    dir.setPath(QString(folderPath));
    dir.setNameFilters(extensionsFilter);

    entryList = dir.entryList();
    for (auto i = entryList.constBegin(); i < entryList.constEnd(); i++){
        QString imagePath = dir.path() + "/" + *i;

        qDebug() << imagePath;
        emit imageFound(imagePath);

        folderImages.push_back(QImage(imagePath));
    }

    return folderImages;
}

std::vector<QImage> ImagesFolder::getGrayscaleImages() {
    if (folderImages.size() == 0){
        folderImages = getImages();
    }

    std::vector<QImage> grayscaleImages;
    for (auto i = folderImages.begin(); i < folderImages.end(); i++) {
        if (i->isGrayscale()) {
            grayscaleImages.push_back(*i);
        }
    }

    return grayscaleImages;
}

const QStringList ImagesFolder::extensionsFilter({
    "*png", "*jpg"
 });
