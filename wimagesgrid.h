#ifndef WIMAGESGRID_H
#define WIMAGESGRID_H

#include <vector>

#include <QGridLayout>
#include <QImage>
#include <QLabel>
#include <QDebug>

#define CONTENT_WIDTH 200
#define CONTENT_HEIGHT 200

#define SCALE_COEFF .2
#define PICTURES_GRID_COLUMNS 5


class wImagesGrid : public QWidget {
    Q_OBJECT
public:
    explicit wImagesGrid(QWidget* parent=0);
    void setImages(std::vector<QImage>);
public slots:
    void onImagesReady(std::vector<QImage>);
private:
    QGridLayout layout;
};

#endif // WIMAGESGRID_H
