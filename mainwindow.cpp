#include "mainwindow.h"
#include "ui_mainwindow.h"

/* Standard lib headers */
#include <iostream>

/* Qt headers */
#include <QString>
#include <QImage>
#include <QFile>
#include <QGraphicsScene>
#include <QPixmap>

/* Constants*/
#define TEST_IMAGE "img_1.png"


MainWindow::MainWindow(QWidget *parent) :
        QMainWindow(parent),
        ui(new Ui::MainWindow) {

    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}
