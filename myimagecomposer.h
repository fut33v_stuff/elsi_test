#ifndef MYIMAGECOMPOSER_H
#define MYIMAGECOMPOSER_H

#include <QColor>

#include "imagecomposer.h"


class MyImageComposer : public ImageComposer {
public:
    MyImageComposer();

    virtual QImage composeTwoImages(QImage, QImage) override;

private:
    QImage composeTwoGrayscale(QImage, QImage, QSize);
    QImage composeTwoRGB(QImage, QImage, QSize);
};

#endif // MYIMAGECOMPOSER_H
