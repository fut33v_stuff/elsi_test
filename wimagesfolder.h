#ifndef WIMAGESFOLDER_H
#define WIMAGESFOLDER_H

#include <QFileDialog>

#include "imagesfolder.h"


class wImagesFolder : public ImagesFolder {
    Q_OBJECT
public:
    explicit wImagesFolder(QWidget* parent=0);
public slots:
    void chooseDirectory();
private:
    QWidget* _parent;
    QFileDialog* fileDialog;
signals:
    void directoryChosen();
};

#endif // WIMAGESFOLDER_H
