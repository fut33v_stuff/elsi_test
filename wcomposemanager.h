#ifndef WCOMPOSEMANAGER_H
#define WCOMPOSEMANAGER_H

#include <QWidget>
#include <QPushButton>
#include <QGridLayout>
#include <QFileInfo>

#include "wimagesfolder.h"
#include "wimagesgrid.h"
#include "myimagecomposer.h"

#define SCALE_TO 300
class wComposeManager : public QWidget {
    Q_OBJECT
public:
    explicit wComposeManager(QWidget *parent = 0);
signals:
    void imagesReady(std::vector<QImage>);
    void imagesComposed(QImage);
public slots:
    void onDirectoryChosen();
    void onMergeButtonClick();
    void onSaveFileButton();
    void onImagesComposed(QImage);
    void onImagesReady();
private:
    std::vector<QImage> images;
    QImage composedImage;

    wImagesGrid imagesGrid;
    wImagesFolder imagesFolder;

    QGridLayout layout;
    QBoxLayout* buttonsLayout;

    QWidget buttons;

    QLabel resultLabel;

    QPushButton* buttonChooseFolder;
    QPushButton* buttonCompose;
    QPushButton* buttonSaveFile;
};

#endif // WCOMPOSEMANAGER_H
