#include "wimagesfolder.h"

wImagesFolder::wImagesFolder(QWidget *parent) {
    this->_parent = parent;
}

void wImagesFolder::chooseDirectory() {
    QString dir = QFileDialog::getExistingDirectory(
        this->_parent, "Open Directory", ".", QFileDialog::ShowDirsOnly
    );
    this->setPath(dir);

    emit directoryChosen();
}
