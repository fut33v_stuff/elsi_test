#-------------------------------------------------
#
# Project created by QtCreator 2015-02-04T16:32:24
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = elsi_test
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    imagesfolder.cpp \
    wimagesfolder.cpp \
    wimagesgrid.cpp \
    imagecomposer.cpp \
    qtimagecomposer.cpp \
    myimagecomposer.cpp \
    wcomposemanager.cpp

HEADERS  += mainwindow.h \
    imagesfolder.h \
    wimagesfolder.h \
    wimagesgrid.h \
    imagecomposer.h \
    qtimagecomposer.h \
    myimagecomposer.h \
    wcomposemanager.h

FORMS    += mainwindow.ui

QMAKE_CXXFLAGS += -std=c++11
