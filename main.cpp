#include "mainwindow.h"
#include <QApplication>

#include <iostream>
#include <string>

#include <QString>
#include <QImage>
#include <QFile>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPainter>
#include <QFileDialog>
#include <QGridLayout>
#include <QScrollArea>
#include <QScrollBar>
#include <QLabel>
#include <QPushButton>

#include "wcomposemanager.h"

#define SCALE_COEFF .2
#define PICTURES_GRID_COLUMNS 5


int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    MainWindow w;

    wComposeManager composeManager(&w);

    w.setCentralWidget(&composeManager);

    w.show();
    return a.exec();
}
