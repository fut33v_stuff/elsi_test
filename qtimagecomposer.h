#ifndef QTIMAGECOMPOSER_H
#define QTIMAGECOMPOSER_H

#include <QPainter>
#include <QDebug>

#include "imagecomposer.h"

class QtImageComposer : public ImageComposer {
public:
    QtImageComposer();
    virtual QImage composeTwoImages(QImage, QImage) override;

    QImage composeImages(std::vector<QImage> images);
};

#endif // QTIMAGECOMPOSER_H
