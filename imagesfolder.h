#ifndef IMAGESFOLDER_H
#define IMAGESFOLDER_H

/* Standard headers */
#include <string>
#include <vector>

/* Qt headers */
#include <QImage>
#include <QDir>
#include <QString>
#include <QStringList>

#include <QDebug>
#include <QObject>


class ImagesFolder : public QObject {
    Q_OBJECT
public:
    ImagesFolder();
    ImagesFolder(QString path);

    void setPath(QString path);
    std::vector<QImage> getImages();
    std::vector<QImage> getGrayscaleImages();
private:
    QString folderPath;
    std::vector<QImage> folderImages;
private:
    /* file extensions that we want to find in folder as image */
    static const QStringList extensionsFilter;
signals:
    void imageFound(QString);
};

#endif // IMAGESFOLDER_H
