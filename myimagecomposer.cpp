#include "myimagecomposer.h"

MyImageComposer::MyImageComposer() {

}

QImage MyImageComposer::composeTwoImages(QImage firstImage, QImage secondImage) {
    QImage result = secondImage;
    QSize s1 = firstImage.size();
    QSize s2 = secondImage.size();


    if (s1 != s2) {
        qDebug() << "Images have different sizes!";
        qDebug() << s1;
        qDebug() << s2;
        return result; //@TODO
    } else {
        if (firstImage.isGrayscale() && secondImage.isGrayscale()){
            return composeTwoGrayscale(firstImage, secondImage, s1);
        } else {
            return composeTwoRGB(firstImage, secondImage, s1);
        }
    }
    return result; //@TODO
}

QImage MyImageComposer::composeTwoGrayscale(QImage firstImage, QImage secondImage, QSize size) {
    firstImage = firstImage.convertToFormat(QImage::Format_Indexed8);
    QImage result(firstImage);
    secondImage = secondImage.convertToFormat(QImage::Format_Indexed8);

    for (int x = 0; x < size.width(); x++){
        for (int y = 0; y < size.height(); y++){
            int i1, i2;
            unsigned int i;

            i1 = firstImage.pixelIndex(x, y);
            i2 = secondImage.pixelIndex(x, y);

            i = i1 * OPACITY + i2 * OPACITY;

            result.setPixel(x, y, i);
        }
    }
    return result;
}

/* That func may be not OK */
QImage MyImageComposer::composeTwoRGB(QImage firstImage, QImage secondImage, QSize size) {
    QImage result = QImage(size, QImage::Format_ARGB32_Premultiplied);
    for (int x = 0; x < size.width(); x++){
        for (int y = 0; y < size.height(); y++){
            int r1, g1, b1, a1, r2, g2, b2, a2;
            int r, g, b;

            QColor c1(firstImage.pixel(x,y));
            QColor c2(secondImage.pixel(x,y));
            c1.getRgb(&r1, &g1, &b1, &a1);
            c2.getRgb(&r2, &g2, &b2, &a2);

            r = r1 * OPACITY + r2 * OPACITY ;
            g = g1 * OPACITY + g2 * OPACITY;
            b = b1 * OPACITY + b2 * OPACITY;

            result.setPixel(x, y,qRgb(r, g, b));
        }
    }
    return result;
}
